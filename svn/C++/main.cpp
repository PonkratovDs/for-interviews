#include <node.hpp>
#include <revision.hpp>
#include <fstream>
#include <sstream>
#include <numeric>
#include  <algorithm>


//ParserActionsToFile - записывает в файл все дерево
void ParserActionsToFile(Revision* rev) {
    std::ofstream fout;
    fout.open("../test/stress_test.txt", std::ios::app);
    if (!fout.is_open()) 
    {
        std::cerr << "file don't open\n";
        fout.close();
    }
    std::stringstream ss;
    ss << "{\"id\" : " << rev->Id << ",\"actions\":[";
    size_t limiter = rev->Actions.size() - 1;
    size_t i = 0;
    for (auto act : rev->Actions) {
        ss << "{\"operation\" : " << act.Operation << ", \"fileAct\" : " 
           << act.FileAct << ", \"type\" : " << act.Type << ", \"fileTo\" : "
           << act.FileTo << "}";
        if (i != limiter) {
            ss << ", ";
        }
        i++;   
    }
    ss << "]}\n";
    fout << ss.str();
    fout.close();
}

// ParsingAction - для каждой папки создает рекурсивно ревизию и вызывает ParserActionsToFile для записи ее в файл
void ParserAction(string currPath, int currId, std::vector<string> folders, std::vector<string> files) {
    Revision* currRevision = new Revision();
    Action currAct;
    currRevision->Actions = std::vector<Action>{};
    for (auto file : files)
    {
        if (currPath == "") 
        {
            currAct.FileAct = "\"" + file + "\"";
        } 
        else
        {
            currAct.FileAct = "\"" + currPath + "/" + file + "\"";
        }
        currAct.Operation = "\"add\"";
        currAct.Type = "\"file\"";
        currAct.FileTo = "\"\"";
        currRevision->Actions.push_back(currAct);
    } 
    for (auto folder : folders)
    {
         if (currPath == "") 
        {
            currAct.FileAct = "\"" + folder + "\"";
        } 
        else
        {
            currAct.FileAct = "\"" + currPath + "/" + folder + "\"";
        }
        currAct.Operation = "\"add\"";
        currAct.Type = "\"folder\"";
        currAct.FileTo = "\"\"";
        currRevision->Actions.push_back(currAct);
    }
    currRevision->Id = currId;
    ParserActionsToFile(currRevision);
}

int main()
{   
    Node* root = new Node("root", std::vector<int>{0}, "NILFile", NILFile, 
        NILNode, "NILNode", NILNode);    
    // ParsingRevisionsToTree(root);
    // printer(root);
    // std::cout << Path(root, "folder2", "4") << "\n";


    std::ofstream fout;
    fout.open("../test/stress_test.txt", std::ios::out);
    fout.close();
    //TODO баг, если делать 3 рекурсные вставки folder1/folder1/file1

    const size_t N = 20;
    int A[N];
    std::iota(A, A+N, 1);
    std::vector<string> folders;
    std::vector<string> files;
    std::for_each(A, A+N-10, [&folders](int x) {string folder = "folder" + std::to_string(x); folders.push_back(folder);} );
    std::for_each(A, A+N, [&files](int x) {string file = "file" + std::to_string(x); files.push_back(file);} );
    string currPath;
    int currId = 1;

    for (int i = 0; i < 100; ++i) {
        ParserAction(currPath, currId, folders, files);
        if (i == 0) 
        {
            currPath = "folder1";
        } else
        {
            currPath += "/folder1";
        }
        currId ++;
    }

    ParserAction(currPath, currId, folders, files);

    File = "../test/stress_test.txt";
    ParsingRevisionsToTree(root);
	return 1;
}