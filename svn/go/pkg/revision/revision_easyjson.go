// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package revision

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson7bc39f0fDecodeSvnPkgRevision(in *jlexer.Lexer, out *Revision) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.Id = int(in.Int())
		case "actions":
			if in.IsNull() {
				in.Skip()
				out.Action = nil
			} else {
				in.Delim('[')
				if out.Action == nil {
					if !in.IsDelim(']') {
						out.Action = make([]Action, 0, 1)
					} else {
						out.Action = []Action{}
					}
				} else {
					out.Action = (out.Action)[:0]
				}
				for !in.IsDelim(']') {
					var v1 Action
					(v1).UnmarshalEasyJSON(in)
					out.Action = append(out.Action, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson7bc39f0fEncodeSvnPkgRevision(out *jwriter.Writer, in Revision) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"id\":"
		out.RawString(prefix[1:])
		out.Int(int(in.Id))
	}
	{
		const prefix string = ",\"actions\":"
		out.RawString(prefix)
		if in.Action == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
			out.RawString("null")
		} else {
			out.RawByte('[')
			for v2, v3 := range in.Action {
				if v2 > 0 {
					out.RawByte(',')
				}
				(v3).MarshalEasyJSON(out)
			}
			out.RawByte(']')
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Revision) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson7bc39f0fEncodeSvnPkgRevision(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Revision) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson7bc39f0fEncodeSvnPkgRevision(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Revision) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson7bc39f0fDecodeSvnPkgRevision(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Revision) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson7bc39f0fDecodeSvnPkgRevision(l, v)
}
func easyjson7bc39f0fDecodeSvnPkgRevision1(in *jlexer.Lexer, out *Action) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "operation":
			out.Operation = string(in.String())
		case "fileAct":
			out.FileAct = string(in.String())
		case "type":
			out.Type = string(in.String())
		case "fileTo":
			out.FileTo = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson7bc39f0fEncodeSvnPkgRevision1(out *jwriter.Writer, in Action) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"operation\":"
		out.RawString(prefix[1:])
		out.String(string(in.Operation))
	}
	{
		const prefix string = ",\"fileAct\":"
		out.RawString(prefix)
		out.String(string(in.FileAct))
	}
	{
		const prefix string = ",\"type\":"
		out.RawString(prefix)
		out.String(string(in.Type))
	}
	{
		const prefix string = ",\"fileTo\":"
		out.RawString(prefix)
		out.String(string(in.FileTo))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Action) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson7bc39f0fEncodeSvnPkgRevision1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Action) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson7bc39f0fEncodeSvnPkgRevision1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Action) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson7bc39f0fDecodeSvnPkgRevision1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Action) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson7bc39f0fDecodeSvnPkgRevision1(l, v)
}
