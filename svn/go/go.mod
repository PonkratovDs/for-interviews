module svn

go 1.14

require (
	9fans.net/go v0.0.2 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/lukehoban/go-find-references v0.0.0-20150918174914-42505ef666d1 // indirect
	github.com/lukehoban/go-outline v0.0.0-20161011150102-e78556874252 // indirect
	github.com/lukehoban/ident v0.0.0-20161021214352-ec6acfba39eb // indirect
	github.com/mailru/easyjson v0.7.1
	github.com/newhook/go-symbols v0.0.0-20151212134530-b75dfefa0d23 // indirect
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836 // indirect
	github.com/petar/GoLLRB v0.0.0-20190514000832-33fb24c13b99 // indirect
	github.com/rogpeppe/godef v1.1.2 // indirect
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/tpng/gopkgs v0.0.0-20180428091733-81e90e22e204 // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/tools v0.0.0-20200527183253-8e7acdbce89d // indirect
	sourcegraph.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
)
