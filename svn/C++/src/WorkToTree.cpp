#include <revision.hpp>
#include <sstream>
#include <set>
#include <functional>
#include <algorithm>

std::vector<int> revisions{};

//TODO посмотреть синтаксис у гугловский пацанов. Переделать циклы, используя algorithm. Убрать скобки, где это не нужно. Для path переделать в std::ostreamstring

//Path - приводит вывод prePath к нужному виду после его выполнения
string Path(Node* root, string path, string interval) {
    prePath(root, path, interval);
    castRightSlice(revisions);
    if (revisions.size() == 0) {
        return "";
    }
    string s;
    for (auto v : revisions) {
        s += " " + std::to_string(v);
    }
    return s.substr(1, s.size());
}

template <class Container>
void split(string& str, Container& cont,
              char delim)
{
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}

//prePath проверяет, что по указанному пути в заданном интервале ревизий все папки к нужной папке/файлу существуют.
//И возвращает нужную строку ревизий.
void prePath(Node* root, string path, string interval) {
    Node* currNode = root;
    int inRev;
    int finRev;

    if (interval != "") {
        std::vector<string> fullInterval;
        split(interval, fullInterval, '-');
        switch (fullInterval.size())
        {
        case 2:
            inRev = std::stoi(fullInterval[0]);
            finRev = std::stoi(fullInterval[1]);
            if (inRev > finRev) {
                exit(int(Error::WrongInputInRevMoreFinRev));
            }
            if (finRev > CURREVISION) {
                exit(int(Error::WrongRequestFinRevMoreCurrRevision));
            }
            break;
        case 1:
            finRev = std::stoi(fullInterval[0]);
            inRev = 1;
            if (inRev > finRev) {
                exit(int(Error::WrongInputInRevMoreFinRev));
            }
            if (finRev > CURREVISION) {
                exit(int(Error::WrongRequestFinRevMoreCurrRevision));
            }
            break;    
        default:
            exit(int(Error::NoValidInterval));
        }
    } else {
        inRev = 1;
        finRev = CURREVISION;
    }
    path = "root/" + path;
    std::vector<string> names;
    split(path, names, '/');
    int limiter = names.size() - 1;
    bool flagForFile = false;
    int i = 0;
    for (auto name : names) {
        if (currNode->Name != name) {
            if (!flagForFile) {
                exit(int(Error::WrongFolderName));
            }
            return;
        }
        if (!NodeExistInInterval(root, currNode, inRev, finRev)) {
            exit(int(Error::FolderDontExistInInterval));
        }
        if (i < limiter) {
            if (!currNode->AvNodes.count(names[i+1])) {
                if (!currNode->Items.count(names[i+1])) {
                    exit(int(Error::FileDontExist));
                } else {
                    if (!FileExistInInterval(currNode->Items[names[i+1]], inRev, finRev)) {
                        exit(int(Error::FileDontExistInInterval));
                    }
                    flagForFile = true;
                    FileGiveRevisions(currNode->Items[names[i+1]], inRev, finRev, revisions);
                    return;
                }
            } else {
                currNode = currNode->AvNodes[names[i+1]];
            }
        }
        i++;
    }
    NodeGiveRevisions(root, currNode, inRev, finRev, revisions);
    return;
}

//NodeExistInInterval - функция проверки, что папка существует на заданном интервале
bool NodeExistInInterval(Node* root, Node* node, int inRev, int finRev) {
    if (node == NILNode) {
        return false;
    }
    bool exist = false;
    if (node == root) {
        exist = true;
        return exist;
    }
    if (node->Existence.size() == 1) {
        int first = node->Existence[0];
        if (finRev >= first) {
            exist = true;
        }
        return exist;
    }
    size_t limiter = node->Existence.size() - 1;
    for (size_t i = 0; i < node->Existence.size(); i += 2) {
        int first = node->Existence[i];
        int last;
        if (i < limiter) {
            last = node->Existence[i+1];
        } else {
            last = first;
        }
        if (finRev >= first && inRev <= last) {
            exist = true;
        }
    }
    return exist;
}

//FileExistInInterval - функция проверки, что файл существует на заданном интервале
bool FileExistInInterval(Item* file, int inRev, int finRev) {
    bool exist = false;
    if (file->Existence.size() == 1) {
        int first = file->Existence[0];
        if (finRev >= first) {
            exist = true;
        }
        return exist;
    }
    size_t limiter = file->Existence.size() - 1;
    for (size_t i = 0; i < file->Existence.size(); i += 2) {
        int first = file->Existence[i];
        int last;
        if (i < limiter) {
            last = file->Existence[i+1];
        } else {
            last = first;
        }
        if (finRev >= first && inRev <= last) {
            exist = true;
        }
    }
    return exist;
}

//FileGiveRevisions - функция добавляет ревизии файла к слайсу ревизий. Учитывает, 
//что файл мог быть скопирован или перемещен
void FileGiveRevisions(Item* file, int inRev, int finRev, std::vector<int>& revisions) {
    if (file == NILFile) {
        return;
    }
    for(auto it = file->History.begin(); it != file->History.end(); ++it) {
        std::pair<int, int> interval = it->first;
        std::vector<int> revs = it->second;
        if (FileExistKeyHistory(interval, inRev, finRev)) {
            for (auto rev : revs) {
                if (rev >= inRev && rev <= finRev) {
                    revisions.push_back(rev);
                }
            }
        }
    }
    if (file->PointerOrigin != NILFile) {
        FileGiveRevisions(file->PointerOrigin, inRev, file->Existence[0], revisions);
    }
}

//NodeGiveRevisions - функция добавляет ревизии всех файлов к слайсу ревизий. 
//Учитывает, что папка могла быть скопирована или перемещена
void NodeGiveRevisions(Node* root, Node* node, int inRev, int finRev, std::vector<int>& revisions) {
    GiveRevisionsInNode(root, node, inRev, finRev, revisions);
    for (auto it = node->Items.begin(); it != node->Items.end(); ++it) {
        FileGiveRevisions(it->second, inRev, finRev, revisions);
    }
    if (node->PointerOrigin != NILNode) {
        NodeGiveRevisions(root, node->PointerOrigin, inRev, node->Existence[0], revisions);
    }
}

//GiveRevisionsInNode - берет информацию о создании папок в данном интервале ревизий и записывает ее в вектор ревизий
void GiveRevisionsInNode(Node* root, Node* node, int inRev, int finRev, std::vector<int>& revisions) {
    if (!NodeExistInInterval(root, node, inRev, finRev)) {
        return;
    }
    for (auto rev : node->Existence) {
        if (inRev <= rev && rev <= finRev) {
            revisions.push_back(rev);
        }
    }
    for (auto it = node->AvNodes.begin(); it != node->AvNodes.end(); ++it) {
        GiveRevisionsInNode(root, it->second, inRev, finRev, revisions);
    }
}

//FileExistHistory проверяет, принадлежит ли заданный ключ в history нужному интервалу ревизий или нет
bool FileExistKeyHistory(std::pair<int, int> key, int inRev, int finRev) {
    if (key.second == 0) {
        if (key.first <= finRev) {
            return true;
        }
    }
    if (finRev >= key.first && inRev <= key.second) {
        return true;
    }
    return false;
}

//castRightSlice удаляет повторяющие элементы и сортирует их в обратной порядке
void castRightSlice(std::vector<int>& revisions) {
    std::vector< int >::iterator i , j ;
    std::set< int > t_set;
    for( i = revisions.begin() , j = revisions.begin() ; i != revisions.end() ; ++i )
        if( t_set.insert( *i ).second) 
            *j++ = *i ;
    revisions.erase( j , revisions.end() ); 
    std::sort(revisions.begin(), revisions.end(), std::greater<int>());
}
