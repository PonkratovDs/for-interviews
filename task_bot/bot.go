package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const (
	BotToken   = os.Getenv("task_bot_token")
	WebhookURL = os.Getenv("task_bot_webhook")
)

type Task struct {
	ID          int
	Description string
	Creator     User
	Executor    User
	Comand      string
	CanResolve  bool
	CanAssign   bool
	CanUnassign bool
}

type User struct {
	ID       int
	LastName string
	UserName string
	ChatID   int64
}

type Message struct {
	ChatId int64
	Text   string
}

var (
	Tasks      map[int]*Task = make(map[int]*Task)
	Users      map[int]User  = make(map[int]User)
	NumCurTask int           = 0
	NilUser                  = User{}
)

// RequestHelp выводит список исполняемых ботом команд
func RequestHelp() string {
	return "`/tasks` - выдает список активных задач\n" +
		"`/new XXX YYY ZZZ` - создаёт новую задачу\n" +
		"`/assign_$ID` - делаеть пользователя исполнителем задачи\n" +
		"`/unassign_$ID` - снимает задачу с текущего исполнителя\n" +
		"`/resolve_$ID` - выполняет задачу, удаляет её из списка\n" +
		"`/my` - показывает задачи, которые назначены на меня\n" +
		"`/owner` - показывает задачи которые были созданы мной"

}

// RequestNew создаёт новую задачу
func RequestNew(id int, cmd string) string {
	NumCurTask++
	description := strings.SplitAfterN(cmd, " ", 2)[1]
	curruser := Users[id]

	var bComand strings.Builder
	fmt.Fprintf(&bComand, "%d. \"%s\" by %s\n", NumCurTask, description, curruser.UserName)

	Tasks[NumCurTask] = &Task{
		ID:          NumCurTask,
		Description: description,
		Creator:     curruser,
		Executor:    NilUser,
		Comand:      bComand.String(),
		CanResolve:  true,
		CanAssign:   true,
		CanUnassign: true,
	}

	var b strings.Builder
	fmt.Fprintf(&b, "Задача \"%s\" создана, id=%d\n", description, NumCurTask)

	return b.String()
}

// RequestAssign делает пользователя исполнителем задачи
func RequestAssign(id int, cmd string) map[string]Message {
	i := strings.SplitAfterN(cmd, "/assign_", 2)[1]
	id_task, err := strconv.Atoi(i)
	if err != nil {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /assign. Наберите /help",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	curruser := Users[id]
	task, ok := Tasks[id_task]
	if !ok {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /assign.  Задачи с ID " + i + " не существует",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	task.Executor = curruser
	task.CanAssign = false
	task.CanUnassign = true
	if task.Executor == task.Creator {
		var b strings.Builder
		fmt.Fprintf(&b, "Задача \"%s\" назначена на вас",
			Tasks[id_task].Description)
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	var (
		bCreatorStr  strings.Builder
		bExecutorStr strings.Builder
	)
	fmt.Fprintf(&bCreatorStr, "Задача \"%s\" назначена на %s",
		Tasks[id_task].Description, curruser.UserName)
	fmt.Fprintf(&bExecutorStr, "Задача \"%s\" назначена на вас",
		Tasks[id_task].Description)
	mCreator := Message{
		ChatId: Tasks[id_task].Creator.ChatID,
		Text:   bCreatorStr.String(),
	}
	mExecutor := Message{
		ChatId: int64(id),
		Text:   bExecutorStr.String(),
	}
	return map[string]Message{

		"Creator":  mCreator,
		"Executor": mExecutor,
	}
}

// RequestUnassign снимает задачу с текущего исполнителя
func RequestUnassign(id int, cmd string) map[string]Message {
	i := strings.SplitAfterN(cmd, "/unassign_", 2)[1]
	id_task, err := strconv.Atoi(i)
	if err != nil {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /unassign. Наберите /help",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	curruser := Users[id]
	task, ok := Tasks[id_task]
	if !ok {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /unassign.  Задачи с ID " + i + " не существует",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	if task.Executor != curruser {
		var b strings.Builder
		fmt.Fprintf(&b, "Задача не на вас")
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	task.CanUnassign = false
	task.CanAssign = true
	if task.Executor == task.Creator {
		var b strings.Builder
		fmt.Fprintf(&b, "Принято")
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		task.Executor = NilUser
		return map[string]Message{
			"Executor": m,
		}
	}
	var (
		bCreatorStr  strings.Builder
		bExecutorStr strings.Builder
	)
	fmt.Fprintf(&bCreatorStr, "Задача \"%s\" осталась без исполнителя", Tasks[id_task].Description)
	fmt.Fprintf(&bExecutorStr, "Принято")
	mCreator := Message{
		ChatId: Tasks[id_task].Creator.ChatID,
		Text:   bCreatorStr.String(),
	}
	mExecutor := Message{
		ChatId: int64(id),
		Text:   bExecutorStr.String(),
	}
	task.Executor = NilUser
	return map[string]Message{

		"Creator":  mCreator,
		"Executor": mExecutor,
	}
}

// RequestResolve выполняет задачу, удаляет её из списка
func RequestResolve(id int, cmd string) map[string]Message {
	i := strings.SplitAfterN(cmd, "/resolve_", 2)[1]
	id_task, err := strconv.Atoi(i)
	if err != nil {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /resolve. Наберите /help",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	curruser := Users[id]
	task, ok := Tasks[id_task]
	if !ok {
		m := Message{
			ChatId: int64(id),
			Text:   "Не валидный запрос для /resolve.  Задачи с ID " + i + " не существует",
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	if task.Executor == NilUser {
		var b strings.Builder
		fmt.Fprintf(&b, "Задача не имеет исполнителя")
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		return map[string]Message{
			"Executor": m,
		}
	}
	if task.Executor != curruser {
		var b strings.Builder
		fmt.Fprintf(&b, "Задача не на вас")
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		return map[string]Message{
			"Executor": m,
		}
	}

	task.CanResolve = false
	if task.Executor == task.Creator {
		var b strings.Builder
		fmt.Fprintf(&b, "Задача \"%s\" выполнена", Tasks[id_task].Description)
		m := Message{
			ChatId: int64(id),
			Text:   b.String(),
		}
		delete(Tasks, id_task)
		return map[string]Message{
			"Executor": m,
		}
	}
	var (
		bCreatorStr  strings.Builder
		bExecutorStr strings.Builder
	)
	fmt.Fprintf(&bCreatorStr, "Задача \"%s\" выполнена %s", Tasks[id_task].Description, curruser.UserName)
	fmt.Fprintf(&bExecutorStr, "Задача \"%s\" выполнена", Tasks[id_task].Description)
	mCreator := Message{
		ChatId: Tasks[id_task].Creator.ChatID,
		Text:   bCreatorStr.String(),
	}
	mExecutor := Message{
		ChatId: int64(id),
		Text:   bExecutorStr.String(),
	}
	delete(Tasks, id_task)
	return map[string]Message{

		"Creator":  mCreator,
		"Executor": mExecutor,
	}
}

// RequestTasks выдает список активных задач
func RequestTasks(id int) string {
	if len(Tasks) == 0 {
		return "Нет задач"
	}
	var comands []string
	for _, task := range Tasks {
		var b strings.Builder
		switch {
		case task.CanAssign:
			fmt.Fprintf(&b, "%s/assign_%d", task.Comand, task.ID)
			comands = append(comands, b.String())
			break
		case !task.CanAssign && task.CanUnassign && task.CanResolve && task.Executor == Users[id]:
			fmt.Fprintf(&b, "%sassignee: я\n/unassign_%d /resolve_%d", task.Comand, task.ID, task.ID)
			comands = append(comands, b.String())
			break
		case !task.CanAssign && task.Executor != Users[id]:
			fmt.Fprintf(&b, "%sassigne: %s", task.Comand, task.Executor.UserName)
			comands = append(comands, b.String())
			break
		}
	}
	comand := ""
	for _, val := range comands {
		comand += val + "\n"
	}
	return comand[:len(comand)-1]
}

// RequestMy показывает задачи, которые назначены на меня
func RequestMy(id int) string {
	if len(Tasks) == 0 {
		return "Нет задач"
	}
	var comands []string
	for _, task := range Tasks {
		var b strings.Builder
		if task.Executor.ID == id {
			switch {
			case task.CanAssign:
				fmt.Fprintf(&b, "%s/assign_%d", task.Comand, task.ID)
				comands = append(comands, b.String())
				break
			case !task.CanAssign && task.CanUnassign && task.CanResolve && task.Executor == Users[id]:
				fmt.Fprintf(&b, "%sassignee: я\n/unassign_%d /resolve_%d", task.Comand, task.ID, task.ID)
				comands = append(comands, b.String())
				break
			case !task.CanAssign && task.Executor != Users[id]:
				fmt.Fprintf(&b, "%sassigne: %s", task.Comand, task.Executor.UserName)
				comands = append(comands, b.String())
				break
			}
		}
	}
	comand := ""
	for _, val := range comands {
		comand += val + "\n"
	}
	if len(comand) == 0 {
		return "Нет задач"
	}
	return comand[:len(comand)-1]
}

// RequestOwner показывает задачи которые были созданы мной
func RequestOwner(id int) string {
	if len(Tasks) == 0 {
		return "Нет задач"
	}
	var comands []string
	for _, task := range Tasks {
		var b strings.Builder
		if task.Creator.ID == id {
			switch {
			case task.CanAssign:
				fmt.Fprintf(&b, "%s/assign_%d", task.Comand, task.ID)
				comands = append(comands, b.String())
				break
			case !task.CanAssign && task.CanUnassign && task.CanResolve && task.Executor == Users[id]:
				fmt.Fprintf(&b, "%sassignee: я\n/unassign_%d /resolve_%d", task.Comand, task.ID, task.ID)
				comands = append(comands, b.String())
				break
			case !task.CanAssign && task.Executor != Users[id]:
				fmt.Fprintf(&b, "%sassigne: %s", task.Comand, task.Executor.UserName)
				comands = append(comands, b.String())
				break
			}
		}
	}
	comand := ""
	for _, val := range comands {
		comand += val + "\n"
	}
	if len(comand) == 0 {
		return "Нет задач"
	}
	return comand[:len(comand)-1]
}

func startTaskBot(ctx context.Context) error {
	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		panic(err)
	}

	// bot.Debug = true
	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)

	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		panic(err)
	}

	updates := bot.ListenForWebhook("/")

	port := os.Getenv("PORT")
	go http.ListenAndServe(":"+port, nil)

	// получаем все обновления из канала updates
	for update := range updates {
		user := update.Message.From
		id := user.ID
		if _, ok := Users[id]; !ok {
			UserName := "@" + user.UserName
			Users[id] = User{
				ID:       user.ID,
				LastName: user.LastName,
				UserName: UserName,
				ChatID:   update.Message.Chat.ID,
			}
		}
		cmd := update.Message.Text
		switch {
		case cmd == "/start":
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Добро пожаловать. Вас приведствует TaskdPBot. Для того, чтобы узать список комад - введите /help"))
		case cmd == "/help":
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, RequestHelp()))
		case strings.Contains(cmd, "/new"):
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, RequestNew(id, cmd)))
		case strings.Contains(cmd, "/assign_"):
			response := RequestAssign(id, cmd)
			SendBot(bot, response)
		case strings.Contains(cmd, "/unassign_"):
			response := RequestUnassign(id, cmd)
			SendBot(bot, response)
		case strings.Contains(cmd, "/resolve_"):
			response := RequestResolve(id, cmd)
			SendBot(bot, response)
		case cmd == "/tasks":
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, RequestTasks(id)))
		case cmd == "/my":
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, RequestMy(id)))
		case cmd == "/owner":
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, RequestOwner(id)))
		default:
			bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Неизвестная или невалидная команда. Для справки наберите /help"))
		}
	}
	return nil
}

// SendBot отправляет ответ пользователю
func SendBot(bot *tgbotapi.BotAPI, response map[string]Message) {
	if _, ok := response["Creator"]; !ok {
		bot.Send(tgbotapi.NewMessage(response["Executor"].ChatId, response["Executor"].Text))
	} else {
		bot.Send(tgbotapi.NewMessage(response["Creator"].ChatId, response["Creator"].Text))
		bot.Send(tgbotapi.NewMessage(response["Executor"].ChatId, response["Executor"].Text))
	}
}

func main() {
	err := startTaskBot(context.Background())
	if err != nil {
		panic(err)
	}
}
