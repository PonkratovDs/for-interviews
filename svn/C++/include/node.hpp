#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <utility>

using std::string;

using history = std::map<std::pair<int,int>, std::vector<int>>; 

struct Item final
{
    string                      Name;
    history                     History;
    Item*                       PointerOrigin;
    std::vector<int>            Existence;

    Item() {}
    Item(string name_, std::pair<int, int> key_history_, std::vector<int> val_history_,  Item* pointerOrigin_, std::vector<int> existence_);

    friend std::ostream& operator<< (std::ostream &out, Item* file);
};

struct Node final
{
    string                       Name;
    std::vector<int>             Existence;
    std::map<string,Item*>       Items;
    Node*                        PointerOrigin;
    std::map<string,Node*>       AvNodes;

    Node() {}
    Node(string name_, std::vector<int> existence_, string key_items_, Item* val_Items_,
        Node* pointerOrigin_, string key_avNodes, Node* val_avNodes);

    friend std::ostream& operator<< (std::ostream &out, Node* folder);    
};

std::ostream& operator<< (std::ostream &out, std::vector<int> vector);
std::ostream& operator<< (std::ostream &out, history& history);
std::ostream& operator<< (std::ostream &out, std::map<string, Item*> Items);
std::ostream& operator<< (std::ostream &out, std::map<string, Node*> avNodes);
