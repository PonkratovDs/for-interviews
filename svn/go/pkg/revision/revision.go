package revision

//easyjson:json
type Revision struct {
	Id     int      `json:"id"`
	Action []Action `json:"actions"`
}

type Action struct {
	Operation string `json:"operation"`
	FileAct   string `json:"fileAct"`
	Type      string `json:"type"`
	FileTo    string `json:"fileTo"`
}

