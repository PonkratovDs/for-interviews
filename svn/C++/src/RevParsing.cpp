#include <revision.hpp>
#include <node.hpp>
#include <json.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator> 

using nlohmann::json;
int CURREVISION = 0;
string File = "../data/mock_revision.txt";

Node* NILNode = nullptr;
Item* NILFile = nullptr;

namespace ns {
    void from_json_act(const json& j, Action& act) {
        j.at("operation").get_to(act.Operation);
        j.at("fileAct").get_to(act.FileAct);
        j.at("type").get_to(act.Type);
        j.at("fileTo").get_to(act.FileTo);
    }
    void from_json_rev(const json& j, Revision& rev) {
        j.at("id").get_to(rev.Id);
        for (auto& element : j.at("actions")) {
            Action* act = new Action();
            from_json_act(element, *act);
            rev.Actions.push_back(*act);
            free(act);
        }
    }
}

bool folderOrFileExist(std::vector<int> existence) {
    return (existence.size()) % 2 != 0;
}

//ParsingRevisionsToTree читает с файла с данными ревизии, парсит их в стуктуру в зависимости от функции (mod, del,
//cp, mv, add) в ревизии. Данные сохраняются в структуре
void ParsingRevisionsToTree(Node* root)
{
    std::string line;
    std::ifstream in(File);
    if (in.is_open())
    {
        std::stringstream s;
        while (getline(in, line))
        {
            auto j = json::parse(line);
            Revision* rev = new Revision();
            ns::from_json_rev(j, *rev);
            CURREVISION ++; 

            for (auto& a : rev->Actions) {
                if (a.Operation == std::string("add")) {
                    addRevision(root, a, rev->Id);
                } else if (a.Operation == std::string("mod")) {
                    modRevision(root, a, rev->Id);
                } else if (a.Operation == std::string("del")) {
                    delRevision(root, a, rev->Id);
                } else if (a.Operation == std::string("cp")) {
                    cpRevision(root, a, rev->Id);
                } else if (a.Operation == std::string("mv")) {
                    mvRevision(root, a, rev->Id);
                } else {
                    exit(int(Error::BadTypeOperation));
                }
            }
        }
    }
    in.close();
}

//addRevision - добавляет ревизию в дерево. Идет по указанному пути в FileAct. Если
//вдруг указанного файла или папки нет по пути, то смотрит на PointerOrigin, если там нет, то выдается
//паника. Также смотрит, чтобы файлы и папки сущестовали в текущей ревизии. Если папка, то создает новую
//папку, если файл, то файл. Если был  PointerToOrigin, то учитывает это. Заметим, что в данной реализации
//автоматически нельзя добавить файл или папку в файл
void addRevision(Node* root, Action& a, int id) {
    std::pair<Node*, std::string> pn = getParentAndName(root, a.FileAct, id);
    Node* parent = pn.first; std::string name = pn.second;
    if (a.Type == "file") {
        Item* pointerOrigin = NILFile;
        if (parent->PointerOrigin != NILNode) {
            pointerOrigin = parent->PointerOrigin->Items[name];
        }
        Item* item = new Item(name, std::make_pair(id, 0), std::vector<int>{id},  pointerOrigin, std::vector<int>{id});
        if (parent->Items.count(name)) {
            if (parent->Items[name] != nullptr) {
                item = parent->Items[name];    
                if (folderOrFileExist(item->Existence)) {
                deleteFile(item, id);
                }
                item->Existence.push_back(id);
                item->History[std::make_pair(id, 0)] = std::vector<int>{id};
            }
        }
        parent->Items[name] = item;
    } else if (a.Type == "folder") {
        Node* pointerOrigin = NILNode;
        if (parent->PointerOrigin != NILNode) {
            pointerOrigin = parent->PointerOrigin->AvNodes[name];
        }
        Node* node = new Node(name, std::vector<int>{id}, "NILFile", NILFile, 
        pointerOrigin, "NILNode", NILNode);
        if (parent->AvNodes.count(name)) {
            node = parent->AvNodes[name];
            if (folderOrFileExist(node->Existence)) {
                deleteFolder(node, id, a.FileAct);
            }
            node->Existence.push_back(id);
        }
        parent->AvNodes[name] = node;
    } else {
        exit(int(Error::BadTypeOperation));
    }
}

//getParentAndName - возвращает предка и имя ноды или файла.
std::pair<Node*, std::string> getParentAndName(Node* root, string path, int id) {
    size_t i = path.find_last_of("/"); //в реализации на ++ не надо проверять на i == -1
    string name = path.substr(i+1);
    Node* parent;
    if (name == path.substr(0, i)) {
        parent = findNode(root, "", id);
    } else {
        parent = findNode(root, path.substr(0, i), id);
    }
    return std::make_pair(parent, name);
}

template <class Container>
void split(string& str, Container& cont,
              char delim = '/')
{
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}

//findNode - ищет предка файла или папки. Учитывает PointerOrigin
Node* findNode(Node* root, string path, int id) {
    std::vector<string> names;
    if (path == "") {
        path = "root";
        names.push_back(path);
    } else {
        path = "root/" + path;
        split(path, names);
    }
    Node* currNode = root;
    size_t limiter = names.size() - 1;
    for (size_t i = 0; i < names.size(); i++) {
        if (!folderOrFileExist(currNode->Existence)) {
            exit(int(Error::NodeDontExistInCurrRevision));
        }
        if (i == limiter) {
            break;
        }
        if (!currNode->AvNodes.count(names[i+1])) {
            if (currNode->PointerOrigin != NILNode) {
                if (currNode->PointerOrigin->Name != names[i+1]) {
                    exit(int(Error::NodeDontExistInOriginNode));
                } else {
                    Node* pointerOrigin = NILNode;
                    if (i + 1 != limiter) {
                        if (currNode->PointerOrigin->AvNodes.count(names[i+2])) {
                            pointerOrigin = currNode->PointerOrigin->AvNodes[names[i+2]];
                        }
                    }
                    Node* newNode = new Node(names[i+1], std::vector<int>{id}, "NILFile", 
                        NILFile, pointerOrigin, "NILNode", NILNode);
                    currNode->AvNodes[names[i+1]] = newNode;    
                }
            } else if (currNode == root && limiter == 1) {
                return root;
            } else {
                exit(int(Error::NodeDontExist));
            }
        }
        currNode = currNode->AvNodes[names[i+1]];
    }
    return currNode;
}


//delFile - функция-помощник, которая просто добавляет информацию об удалении в Existence, а также делает новый ключ History,
//копируя историю старого. Удаляет старый ключ
void deleteFile(Item* delFile, int id) {
    int k = delFile->Existence[delFile->Existence.size() - 1];
    delFile->Existence.push_back(id);
    delFile->History[std::make_pair(k, id)] = delFile->History[std::make_pair(k, 0)];
    delFile->History.erase(std::make_pair(k, 0));
    delFile->History[std::make_pair(k, id)].push_back(id);
}

//deleteFolder - функция-помошник, которая добовляет информацию об удалении в Existence папка, потом итерируется по файлам,
//вызывая delFile. А далее рекурсивно вызывает данную функцию для всех доступных папок
void deleteFolder(Node* delFolder, int id, string path) {
    if (!folderOrFileExist(delFolder->Existence)) {
        exit(int(Error::NodeDontExistInCurrRevision));
    }
    delFolder->Existence.push_back(id);
    for (auto it = delFolder->Items.begin(); it != delFolder->Items.end(); ++it) {
        if (it->second != NILFile) {
            if (folderOrFileExist(it->second->Existence)) {
                deleteFile(it->second, id);
            }
        }
    }
    for (auto it = delFolder->AvNodes.begin(); it != delFolder->AvNodes.end(); ++it) {
        if (it->second != NILNode) {
            string pathFolder = path + "/" + it->second->Name;
            deleteFolder(it->second, id, pathFolder);
        }
    }
}

//delRevision - удаляет файл или рекурсивно папку, с доступными ей папками и файлами. Если файл, то просто обновляем Item
//файла, если он уже не удален. Если папка, то делаем запись в текущую папку об удалении, удаляем все файлы и рекурсивно
//удаляем доступные папки с их файлами. Учитываем, что папки и/или файлы уже могут быть удалены
void delRevision(Node* root, Action& a, int id) {
    std::pair<Node*, std::string> pn = getParentAndName(root, a.FileAct, id);
    Node* parent = pn.first; std::string name = pn.second;
    if (parent->Items.count(name)) {
        Item* delFile = parent->Items[name];
        if (!folderOrFileExist(delFile->Existence)) {
            exit(int(Error::ObjectDoesNotExistInCurrRevision));
        }
        deleteFile(delFile, id);
    } else if (parent->AvNodes.count(name)) {
        Node* delFolder = parent->AvNodes[name];
        deleteFolder(delFolder, id, a.FileAct);
    } else {
        exit(int(Error::ObjectDoesNotExist));
    }
}

//modRevision - модифицирует файл. Операции mod к папке быть не может. Делает find по пути. Далее просто обновляет Item файла
void modRevision(Node* root, Action& a, int id) {
    std::pair<Node*, std::string> pn = getParentAndName(root, a.FileAct, id);
    Node* parent = pn.first; std::string name = pn.second;
    if (!parent->Items.count(name)) {
        exit(int(Error::ObjectDoesNotExistInCurrRevision));
    }
    Item* modFile = parent->Items[name];
    if (!folderOrFileExist(modFile->Existence)) {
            exit(int(Error::ObjectDoesNotExistInCurrRevision));
    }
    int k = modFile->Existence[modFile->Existence.size() - 1];
    modFile->History[std::make_pair(k, 0)].push_back(id);
}

//cpRevision - копирует файл или папку. Делаем find для папки от куда копируем. Получаем копируемую ноду или файл.
//Делаем find по пути для папки, куда копируем, либо для файла, куда копируем. Если файл копиркуется в файл, то
//добавляем новую запись в item. При этом наследуем историю, как того файла, так и этого. Если файл в папку, то
//создаем новый файл, у которого будет будет ссылка на тот, из которого копировали. Копировать папку в файл нельзя.
//Если копируем папку в папку, то просто кладем в родителя исходной папки указатель на новую папку, в которой есть
//указатель на исходную папку
void cpRevision(Node* root, Action& a, int id) {
    std::pair<Node*, std::string> pnAct = getParentAndName(root, a.FileAct, id);
    Node* parentAct = pnAct.first; std::string nameAct = pnAct.second;
    std::pair<Node*, std::string> pnTo = getParentAndName(root, a.FileTo, id);
    Node* parentTo = pnTo.first; std::string nameTo = pnTo.second;

    bool okActFile = static_cast<bool>(parentAct->Items.count(nameAct));
    bool okActNode = static_cast<bool>(parentAct->AvNodes.count(nameAct));
    bool okToFile = static_cast<bool>(parentTo->Items.count(nameTo));
    bool okToNode = static_cast<bool>(parentTo->AvNodes.count(nameTo));

    if (okActFile && okToFile) {
        Item* ActFile = parentAct->Items[nameAct];
        Item* ToFile = parentTo->Items[nameTo];
        if (folderOrFileExist(ActFile->Existence) && folderOrFileExist(ToFile->Existence)) {
            // for (auto it = ActFile->History.begin(); it != ActFile->History.end(); ++it) {
            //     if (ToFile->History.count(it->first)) {
            //         std::copy(it->second.begin(), it->second.end(), std::back_inserter(ToFile->History[it->first]));
            //     } else {
            //         ToFile->History[it->first] = it->second;
            //     }
            // }
            int k = ToFile->Existence[ToFile->Existence.size() - 1];
            ToFile->History[std::make_pair(k, 0)].push_back(id);
            ToFile->PointerOrigin = ActFile;
            return;
        }
        exit(int(Error::FileDontExistInTheCurrentRevision));
    } else if (okActFile && okToNode) {
        Item* ActFile = parentAct->Items[nameAct];
        Node* ToFolder = parentTo->AvNodes[nameTo];
        if (folderOrFileExist(ActFile->Existence) && folderOrFileExist(ToFolder->Existence)) {
            Item* item = new Item(ActFile->Name, std::make_pair(id, 0), std::vector<int>{id}, ActFile, std::vector<int>{id});
            ToFolder->Items[item->Name] = item;
            return;
        }
        exit(int(Error::FileDontExistInTheCurrentRevision));
    } else if (okActNode && okToNode) {
        Node* ActFolder = parentAct->AvNodes[nameAct];
        Node* ToFolder = parentTo->AvNodes[nameTo];
        if (folderOrFileExist(ActFolder->Existence) && folderOrFileExist(ToFolder->Existence)) {
            Node* newFolder = new Node(ActFolder->Name, std::vector<int>{id}, "NILFile", NILFile, 
                ActFolder, "NILNode", NILNode);
            ToFolder->AvNodes[newFolder->Name] = newFolder;
            return;
        }
        exit(int(Error::FileDontExistInTheCurrentRevision));
    } else if (okActNode && okToFile) {
        exit(int(Error::ConnotCopyFolderToFile));
    } else {
        exit(int(Error::UnforeseenBehavior));
    }
}

//mvRevision - перемещает папку или файл. Делает cp и del для исходного файла или папки. Ограничения, как и в cp
void mvRevision(Node* root, Action& a, int id) {
    cpRevision(root, a, id);
    delRevision(root, a, id);
}

//TODO исправить все warnings

template <typename T>
void helper(std::map<string, T> object) {
    int limiter = object.size() - 1;
    int i = 0;
    if (limiter == -1) {
        std::cout << "\n";
        return;
    }
    for (auto it = object.begin(); it != object.end(); ++it) {
        if (i == limiter) {
            std::cout << it->first << "\n";
            break;
        }
        std::cout << it->first << ", ";
        i++;
    }
}

void recprinter(Node* currNode, string path) {
    if (currNode == NILNode) {
        return;
    } 
    if (!folderOrFileExist(currNode->Existence)) {
        std::cout << "DELETE\n";
    }
    std::cout <<"Path for node: " << path << "\n";
    std::cout << "Name node: " << currNode->Name << "\n";
    std::cout << "Existence: " << currNode->Existence << "\n";
    if (currNode->PointerOrigin != NILNode) {
        std::cout << "Name origin node: " << currNode->PointerOrigin->Name << "\n";
    } else {
        std::cout << "Name origin node: " << "NILNode" << "\n";
    }
    std::cout << "Items: ";
    helper(currNode->Items);
    std::cout << "AvNodes: ";
    helper(currNode->AvNodes);
    
    std::cout << "\nItems for folder with name " << currNode->Name << "\n\n";
    for (auto it = currNode->Items.begin(); it != currNode->Items.end(); ++it) {
        std::cout << it->second;
    }

    for (auto it = currNode->AvNodes.begin(); it != currNode->AvNodes.end(); ++it) {
        if (it->second == nullptr) {
            recprinter(it->second, path); 
        } else {
            std::stringstream newPath("");
            newPath << path << "/" << it->second->Name;
            recprinter(it->second, newPath.str());
        }
    }
}

void printer(Node* currNode) {
    string path = currNode->Name;
    std::cout << "STRUCTURE TREE\n\n";
    recprinter(currNode, path);
    std::cout << "END STRUCTURE TREE\n";
}
