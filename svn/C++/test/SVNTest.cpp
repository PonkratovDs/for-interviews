#include <gtest/gtest.h>
#include <node.hpp>
#include <revision.hpp>
#include <json.hpp>
#include <fstream>

using nlohmann::json;

struct SVNTest : public ::testing::Test {
    Node* root;
    virtual void SetUp() override {
        root = new Node("root", std::vector<int>{0}, "NILFile", NILFile, 
            NILNode, "NILNode", NILNode);
    }
    virtual void TearDown() override {
        delete root;
        revisions.clear();
    }
};

using SVNDeathTest = SVNTest;

TEST_F(SVNTest, SmokeTest) {
    File = "../../data/mock_revision.txt";
    ParsingRevisionsToTree(root);
    EXPECT_EQ(Path(root, "folder2", ""), "4 3"); 
}

TEST_F(SVNTest, AddRevisionTestSucces) {
    File = "../../test/add_revision_test.txt";
    ParsingRevisionsToTree(root);
    EXPECT_EQ(Path(root, "folderAdd", "1"), "1") << "just insert folder";  //просто вставка папки
    revisions.clear();
    EXPECT_EQ(Path(root, "folderAdd/fileAdd", "1"), "1") << "just insert file"; //просто вставка файла в папку
    revisions.clear();
    EXPECT_EQ(Path(root, "folderAdd/folderCp/fileAddCp", "2-4"), "4") << "test to add a file to the copied directory"; //тест на добавление файла в скопированный каталог. 2-4, потому что 
                                                                       //интересующие нас действия находятся в этом интервале. Не подгонка
    revisions.clear();
    EXPECT_EQ(Path(root, "folderAdd/folderCp/folderAddCp", "2-4"), "4") << "test to add a folder to the copied directory"; //тест на добавление папки в скопированный каталог.
    revisions.clear();
    EXPECT_EQ(Path(root, "folderCp", "2-5"), "5 2") << "test to add an existing folder";  //тест на добавление уже существующей папки
    revisions.clear();
    EXPECT_EQ(Path(root, "folderAdd/fileAdd", "1-6"), "6 1") << "test to add an existing file"; //тест на добавление уже существующего файла
}



// TEST_F(SVNDeathTest, AddRevisionTestNodeDontExist) {
//     File = "../../data/add_revision_folderDontExist_test.txt";
//     ParsingRevisionsToTree(root);
    
//     EXPECT_EXIT(Path(root, "folderCp/fileAdd", ""), ::testing::ExitedWithCode(02), "");
// }

// TODO  понять, почему фиговина выше не работает (тест с добавлением файла в несуществующий каталог), хотя с нижней все ок. GDB выводит, что значение 2
// Далее должны быть DEATH тесты для addRevision с  добавлением папки в несуществующий каталог; добавление папки в файл; добавлением файла в файл

// void ExitingFunction() {
//   std::cerr << "[MR3432] Too bad, we are exiting";
//   exit(0);
// }

// TEST(FirstDeathTest, KillingAround) {
//   EXPECT_EXIT(ExitingFunction(), ::testing::ExitedWithCode(0),
// 	      "[MR*] *");
// }

TEST_F(SVNTest, ModRevisionTestSucces) {
    File = "../../test/mod_revision_test.txt";
    ParsingRevisionsToTree(root);
    EXPECT_EQ(Path(root, "folder1/file1"), "2 1") << "just modification file";  //просто модицикация файла
}

TEST_F(SVNTest, DelRevisionTestSucces) {
    File = "../../test/del_revision_test.txt";
    ParsingRevisionsToTree(root);
    EXPECT_EQ(Path(root, "folder1/file1"), "3 2 1") << "just deletion file";  //просто удаление файла
    revisions.clear();
    EXPECT_EQ(Path(root, "folder1"), "4 3 2 1") << "just deletion folder"; //просто удаление папки
}

TEST_F(SVNTest, CpRevisionTestSucces) {
    File = "../../test/cp_revision_test.txt";
    ParsingRevisionsToTree(root);
    EXPECT_EQ(Path(root, "folder2/folder1"), "4 2 1") << "just copying folder";  //просто копирование папки
    revisions.clear();
    EXPECT_EQ(Path(root, "folder2/file1"), "5 2 1") << "just copying file";  //просто копирование файла
    revisions.clear();
    EXPECT_EQ(Path(root, "folder4"), "8 7") << "test series: folder 1 is copied to folder 2. Folder 2 to folder 1. Make sure that there is no ring uncertainty";  
                                                //серия тестов: папка 1 копируется в папку 2. Папка 2 в папку 1. Следим, чтобы не было кольцевой неопределенности
    revisions.clear();
    EXPECT_EQ(Path(root, "folder3"), "9 6");
    revisions.clear();
    EXPECT_EQ(Path(root, "folder3/folder4"), "9 8 7");
    revisions.clear();
    EXPECT_EQ(Path(root, "folder4/folder3"), "8 6");
    revisions.clear();
    EXPECT_EQ(Path(root, "folder5/file2"), "12") << "a series of tests to verify that changes to the text of the copied file are taken into account"; 
                                                    //серия тестов на проверку того, что изменения текста копируемого файла учитывается 
    revisions.clear();
    EXPECT_EQ(Path(root, "file2"), "13 10"); //здесь казалось бы могло быть 13 12 10, но это не так, ибо файл был скопирован на 13-ой ревизии, а нам интересна
                                             //история, пока еще файл не был скопирован и история после копирования. Если не различать - будет наложение
}

//Тесты для mv фактически не имеют смысла, ибо mv полностью определяется из cp и del

// TEST_F(SVNTest, StressTest) {
//     std::ofstream fout;
//     fout.open("stress_test.txt", std::ios::out);
//     if (!fout.is_open()) {
//         std::cerr << "file don't open\n";
//         fout.close();
//         FAIL();
//     }
//     //TODO дописать парсер структуры в json 
//     fout.close();

// }

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}