#include <node.hpp>

Item::Item(string name_, std::pair<int, int> key_history_, std::vector<int> val_history_,  Item* pointerOrigin_, std::vector<int> existence_) {
    this->Name          = name_;
    this->History       = {{key_history_, val_history_}};
    this->Existence     = existence_;
    this->PointerOrigin = pointerOrigin_;
}

std::ostream& operator<< (std::ostream &out, std::vector<int> vector) {
    size_t limiter = vector.size() - 1;
    size_t i = 0;
    for (auto el : vector) {
        if (i == limiter) {
            out << el;
            break;
        }
        out << el << ", ";
        i++;
    }
    return out;
}

std::ostream& operator<< (std::ostream &out, history& history) {
    for (auto it = history.begin(); it != history.end(); ++it) {
        out << "[" << it->first.first << " , " << it->first.second<< "] : " << "{ " 
        << it->second << " }";
    }
    return out;
}


std::ostream& operator<< (std::ostream &out, Item* file) {
    if (file == nullptr) {
        return out;
    }
    if (file->Existence.size() % 2 == 0) {
        out << "DELETE\n";
    }
    out << "File with name: " << file->Name << "\n";
    out << "History: " << file->History << "\n";
    if (file->PointerOrigin == nullptr) {
        out << "Pointer to origin: " << "NILFile\n";
    } else {
        out << "Pointer to origin: " << file->PointerOrigin->Name << "\n";
    }
    out << "Existence: " << file->Existence << "\n\n";
    return out;
}

std::ostream& operator<< (std::ostream &out, std::map<string, Item*> Items) {
    if (Items.size() != 0) {
        for (auto it = Items.begin(); it != Items.end(); ++it) {
        out << "[" << it->first << "] : " << "File: " << it->second;
        }
    }
    return out;
}

std::ostream& operator<< (std::ostream &out, std::map<string, Node*> avNodes) {
    for (auto it = avNodes.begin(); it != avNodes.end(); ++it) {
        out << "[" << it->first << "] : " << "Folder: " << it->second;
    }
    return out;
}

std::ostream& operator<< (std::ostream &out, Node* folder) {
    if (folder->Existence.size() % 2 == 0) {
        out << "DELETE\n";
    }
    out << "Folder with name\t" << folder->Name << "\n";
    out << "Existence\t" << folder->Existence << "\n";
    if (folder->Items.size() == 0) {
        out << "Items\t\n";
    } else {
        out << "Items\t" << folder->Items << "\n";
    }
    if (folder->PointerOrigin == nullptr) {
        out << "Pointer to origin\t" << "NILFolder\n";
    } else {
        out << "Pointer to origin\t" << folder->PointerOrigin << "\n";
    }
    if (folder->AvNodes.size() == 0) {
        out << "AvNodes\t\n";
    } else {
        out << "AvNodes\t" << folder->AvNodes << "\n\n";
    }
    return out;

}

Node::Node(string name_, std::vector<int> existence_, string key_items_, Item* val_items_, 
    Node* pointerOrigin_, string key_avNodes, Node* val_avNodes) {
    this->Name = name_;
    this->Existence = existence_;
    if (key_items_ == "NILFile") {
        this->Items = std::map<string,Item*> {};
    } else {
        this->Items = {{key_items_, val_items_}};
    }
    if (key_avNodes == "NILNode") {
        this->AvNodes = {};
    } else {
        this->AvNodes = {{key_avNodes, val_avNodes}};
    }
    this->PointerOrigin = pointerOrigin_;
}
