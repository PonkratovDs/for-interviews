package filesystem

type Node struct {
	Name             string
	Existence        []int
	Items            map[string]*Item
	PointerOrigin    *Node					//заметим, что указывает не на оригинал этой структуры, а на ребенка
	AvNodes          map[string]*Node
}

type Item struct {
	Name             string
	History          map[[2]int][]int
	PointerOrigin    *Item
	Existence        []int
}
