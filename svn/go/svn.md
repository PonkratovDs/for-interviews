Дерево состоит из нод. Нода представляет собой:

struct node {
    path            string
    name            string
    existence       []int
    items           map[string]*item
    pathToOrigin    string
    avNodes         map[string]*node
}

где pathToOrigin - полный путь на оригинальную папку, история которой наследуется. Если папка не имеет путь на оригинальную структуру, то здесь "".
avNodes - доступные ноды. Ключи - их имена

items имеет ключи из имен файлов и значения из структуры item

struct item {
    name            string
    history         map[int][]int
    pathToOrigin    string
    existence       []int
}

где pathToOrigin - полный путь на оригинальный файл, история которого наследуется. Если файл не имеет путь на оригинальный файл, то здесь "". 
pathToOrigin нужен для быстрого копирования файла без участия папок

Почему это работает? Допустим у нас есть папки f1 и f2. В f1 есть файлы file1 и  file2. В f2 file3. Они созданы в 4-ой ревизии. Допустим изменялись в 5 и 6-ой.

Копируем f1 в f2. Допустим это была 7-я ревизия Для этого в f2/f1 создаем

node {
    path:           "f2/f1",
    name:           "f1",
    existance:      [7],
    items:          ["file1": item {
                        history:      {0:[7]},
                        existance:    [7],
                        pathToOrigin: ""
                    },
                    "file2": item {
                        history:      {0:[7]},
                        existance:    [7],
                        pathToOrigin: ""
                    }],
    pathToOrigin:   "f1",
    avNodes:        []
}

Теперь изменим файл file3 в 8-ой ревизии. И вооще переместим папку f2 в папку f1/f3 в 9-ой ревизии. Переместим f1/file1 в f1/f3/file1, скопируем f1/file2 в f1/f3/file2 в 10-ой.

Тогда для f1:

node {
    path:           "f1",
    name:           "f1",
    existance:      [4],
    items:          ["file1": item {
                        history {10:[4,5,6,10]},
                        existance [4,10],
                        pathToOrigin: ""
                    },
                    "file2": item {
                        history {0:[4,5,6]},
                        existance [4],
                        pathToOrigin: ""
                    }],
    pathToOrigin:   "",
    avNodes:        ["f3": *node_f1_f3, "f2": *node_f1_f2]
}

f2:

node {
    path:           "f2",
    name:           "f2",
    existance:      [6,9],
    items:          ["file3": item {
                        history {9:[6,8,9]},
                        existance [6,9],
                        pathToOrigin: ""
                    }]
    pathToOrigin:   "",
    avNodes:        [*node_f2_f1]
}

f1/f3:

node {
    path:           "f1/f3",
    name:           "f3",
    existance:      [9],
    items:          ["file1": item {
                        history {0:[9,10]},
                        existance [9],
                        pathToOrigin: "f1_file1"
                    },
                    "file2": item {
                        history {0:[9,10]},
                        existance [9],
                        pathToOrigin: ""
                    }],
    pathToOrigin:   "f2",
    avNodes:        []
}

f2/f1:

node {
    path            "f2/f1",
    name:           "f1",
    existance:      [7,9],
    items:          ["file1": item {
                        history {9:[7,9]},
                        existance [7,9],
                        pathToOrigin: ""
                    },
                    "file2": item {
                        history {9:[7,9]},
                        existance [7,9],
                        ptrToOrigin: ""
                    }],
    pathToOrigin:   "f1",
    avNodes:        []
}


Выполним path(x), где x:

1) f1 - 10,6,5,4
2) f2/f1 - 7,9 в оригинальной. path(node.pathToOrigin, "1-5") вернет 6,5,4. Полный 9,7,6,5,4
3) f1/f3/file1 - 9,10. path(item.pathToOrigin, "1-8") вернет 4,5,6. Полный 10,9,6,5,4

Ясное дело, что path(x, "b-a") по аналогии сделать не сложно. Просто нужно отслеживать границы.