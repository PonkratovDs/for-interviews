package revision

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"svn/pkg/filesystem"
)

const filePath string = "data/mock_revision.txt"

var NILNode *filesystem.Node = &filesystem.Node{Name: "NILNode"}
var NILFile *filesystem.Item = &filesystem.Item{Name: "NILFile"}
var CURREVISION int

//ParsingRevisionsToTree читает с файла с данными ревизии, парсит их в стуктуру в зависимости от функции (mod, del,
//cp, mv, add) в ревизии. Данные сохраняются в структуре
func ParsingRevisionsToTree(root *filesystem.Node) {
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatalf("don't open file with path's %s\n", filePath)
	}
	r := bufio.NewReader(file)
	rev := &Revision{}
	for {
		data, errR := r.ReadBytes('\n')
		if errR != nil {
			if errR == io.EOF {
				break
			}
			log.Fatalf("some problem in read file %s\n", errR)
			return
		}
		errJs := rev.UnmarshalJSON(data)
		if errJs != nil {
			log.Fatalf("some problem in json %s\n", errJs)
		}
		CURREVISION += 1
		for _, a := range rev.Action {
			switch a.Operation {
			case "add":
				addRevision(root, &a, rev.Id)
			case "mod":
				modRevision(root, &a, rev.Id)
			case "del":
				delRevision(root, &a, rev.Id)
			case "cp":
				cpRevision(root, &a, rev.Id)
			case "mv":
				mvRevision(root, &a, rev.Id)
			default:
				log.Fatalf("don't know type operation with name %s\n and ID %d", a.Operation, rev.Id)
			}
		}
	}
}

//addRevision - добавляет ревизию в дерево. Идет по указанному пути в FileAct. Если
//вдруг указанного файла или папки нет по пути, то смотрит на PointerOrigin, если там нет, то выдается
//паника. Также смотрит, чтобы файлы и папки сущестовали в текущей ревизии. Если папка, то создает новую
//папку, если файл, то файл. Если был  PointerToOrigin, то учитывает это. Заметим, что в данной реализации
//автоматически нельзя добавить файл или папку в файл
func addRevision(root *filesystem.Node, a *Action, id int) {
	parent, name := getParentAndName(root, a.FileAct, id)
	switch a.Type {
	case "file":
		pointerOrigin := NILFile
		if parent.PointerOrigin != NILNode {
			pointerOrigin = parent.PointerOrigin.Items[name]
		}
		item := &filesystem.Item{
			Name: name,
			History: map[[2]int][]int{
				[2]int{id, 0}: []int{id},
			},
			PointerOrigin: pointerOrigin,
			Existence:     []int{id},
		}
		if _, ok := parent.Items[name]; ok{ 			//случай, если файл с таким названием уже существовал
			item = parent.Items[name]
			if folderOrFileExist(item.Existence) { 				//если существует в данный момент - удаляем и создаем снова.
				deleteFile(item, id)
			}
			item.Existence = append(item.Existence, id)
			item.History[[2]int{id, 0}] = []int{id}
		}
		parent.Items[name] = item
	case "folder":
		pointerOrigin := NILNode
		if parent.PointerOrigin != NILNode {
			pointerOrigin = parent.PointerOrigin.AvNodes[name]
		}
		node := &filesystem.Node{
			Name:          name,
			Existence:     []int{id},
			Items:         map[string]*filesystem.Item{},
			PointerOrigin: pointerOrigin,
			AvNodes:       map[string]*filesystem.Node{},
		}
		if _, ok := parent.AvNodes[name]; ok{ 			//случай, если папка с таким названием уже существовала
			node = parent.AvNodes[name]
			if folderOrFileExist(node.Existence) {				//если существует папка сейчас, то удаляем и создаем вновь, но
				deleteFolder(node, id, a.FileAct)		//не создаем папки и файлы в ней снова
			}
			node.Existence = append(node.Existence, id)
		}
		parent.AvNodes[name] = node
	default:
		log.Fatalf("don't know type with name %s for file %s\n", a.Type, a.FileAct)
	}
}

//folderOrFileExist - проверяет, что папка или файл существует
func folderOrFileExist(existence []int) bool {
	return len(existence)%2 != 0
}

//getParentAndName - возвращает предка и имя ноды или файла.
func getParentAndName(root *filesystem.Node, path string, id int) (*filesystem.Node, string) {
	i := strings.LastIndexByte(path, '/')
	if i != -1 {
		name := path[i+1 : len(path)]
		parent, err := findNode(root, path[0:i], id)
		if err != nil {
			log.Fatal(err)
		}
		return parent, name
	} else {
		name := path
		parent, err := findNode(root, "", id)
		if err != nil {
			log.Fatal(err)
		}
		return parent, name
	}
}

//findNode - ищет предка файла или папки. Учитывает PointerOrigin
func findNode(root *filesystem.Node, path string, id int) (*filesystem.Node, error) {
	var names []string
	if path == "" {
		path = "root"
		names = []string{path}
	} else {
		path = "root/" + path
		names = strings.Split(path, "/")
	}
	currNode := root
	limiter := len(names) - 1
	for i, _ := range names {
		if !folderOrFileExist(currNode.Existence) {
			return nil, fmt.Errorf("node with name %s don't exist in curr revision", currNode.Name)
		}
		if i == limiter {
			break
		}
		if _, ok := currNode.AvNodes[names[i+1]]; !ok {
			if currNode.PointerOrigin != NILNode {
				if currNode.PointerOrigin.Name != names[i+1] {
					return nil, fmt.Errorf("node with name %s don't exist in origin node %s", names[i+1], currNode.PointerOrigin.Name)
				} else {
					pointerOrigin := NILNode
					if i + 1 != limiter {
						if _, ok := currNode.PointerOrigin.AvNodes[names[i+2]]; ok {
							pointerOrigin = currNode.PointerOrigin.AvNodes[names[i+2]]
						}
					}
					newNode := &filesystem.Node{
						Name:          names[i+1],
						Existence:     []int{id},
						Items:         map[string]*filesystem.Item{},
						PointerOrigin: pointerOrigin,
						AvNodes:       map[string]*filesystem.Node{},
					}
					currNode.AvNodes[names[i+1]] = newNode
				}
			} else if currNode == root && limiter == 1 {
				return root, nil
			} else {
				return nil, fmt.Errorf("node with name %s don't exist", currNode.Name)
			}
		}
		currNode = currNode.AvNodes[names[i+1]]
	}
	return currNode, nil
}

//modRevision - модифицирует файл. Операции mod к папке быть не может. Делает find по пути. Далее просто обновляет Item файла
func modRevision(root *filesystem.Node, a *Action, id int) {
	parent, name := getParentAndName(root, a.FileAct, id)
	if _, ok := parent.Items[name]; !ok {
		log.Fatalf("an object with a location along the path %s does not exist or is a folder", a.FileAct)
	}
	modFile := parent.Items[name]
	if !folderOrFileExist(modFile.Existence) {
		log.Fatalf("an object with a location along the path %s does not exist in the current revision", a.FileAct)
	}
	k := modFile.Existence[len(modFile.Existence) - 1]
	modFile.History[[2]int{k, 0}] = append(modFile.History[[2]int{k, 0}], id)
}

//delRevision - удаляет файл или рекурсивно папку, с доступными ей папками и файлами. Если файл, то просто обновляем Item
//файла, если он уже не удален. Если папка, то делаем запись в текущую папку об удалении, удаляем все файлы и рекурсивно
//удаляем доступные папки с их файлами. Учитываем, что папки и/или файлы уже могут быть удалены
func delRevision(root *filesystem.Node, a *Action, id int) {
	parent, name := getParentAndName(root, a.FileAct, id)
	if _, ok := parent.Items[name]; ok {
		delFile := parent.Items[name]
		if !folderOrFileExist(delFile.Existence) {
			log.Fatalf("an object with a location along the path %s does not exist in the current revision", a.FileAct)
		}
		deleteFile(delFile, id)
	} else if _, ok := parent.AvNodes[name]; ok {
		delFolder := parent.AvNodes[name]
		deleteFolder(delFolder, id, a.FileAct)
	} else {
		log.Fatalf("an object with a location along the path %s does not exist", a.FileAct)
	}
}

//delFile - функция-помощник, которая просто добавляет информацию об удалении в Existence, а также делает новый ключ History,
//копируя историю старого. Удаляет старый ключ
func deleteFile(delFile *filesystem.Item, id int) {
	k := delFile.Existence[len(delFile.Existence)-1]
	delFile.Existence = append(delFile.Existence, id)
	delFile.History[[2]int{k, id}] = delFile.History[[2]int{k, 0}]
	delete(delFile.History, [2]int{k, 0})
	delFile.History[[2]int{k, id}] = append(delFile.History[[2]int{k, id}], id)
}

//deleteFolder - функция-помошник, которая добовляет информацию об удалении в Existence папка, потом итерируется по файлам,
//вызывая delFile. А далее рекурсивно вызывает данную функцию для всех доступных папок
func deleteFolder(delFolder *filesystem.Node, id int, path string) {
	if !folderOrFileExist(delFolder.Existence) {
		log.Printf("a folder with a location along the path %s does not exist in the current revision", path)
		return
	}
	delFolder.Existence = append(delFolder.Existence, id)
	for _, file := range delFolder.Items {
		if !folderOrFileExist(file.Existence) {
			log.Printf("a file with a location along the path %s does not exist in the current revision", path)
			return
		}
		deleteFile(file, id)
	}

	for _, avFolder := range delFolder.AvNodes {
		pathFolder := path + "/" + avFolder.Name
		deleteFolder(avFolder, id, pathFolder)
	}
}

//cpRevision - копирует файл или папку. Делаем find для папки от куда копируем. Получаем копируемую ноду или файл.
//Делаем find по пути для папки, куда копируем, либо для файла, куда копируем. Если файл копиркуется в файл, то
//добавляем новую запись в item. При этом наследуем историю, как того файла, так и этого. Если файл в папку, то
//создаем новый файл, у которого будет будет ссылка на тот, из которого копировали. Копировать папку в файл нельзя.
//Если копируем папку в папку, то просто кладем в родителя исходной папки указатель на новую папку, в которой есть
//указатель на исходную папку
func cpRevision(root *filesystem.Node, a *Action, id int) {
	parentAct, nameAct := getParentAndName(root, a.FileAct, id)
	parentTo, nameTo := getParentAndName(root, a.FileTo, id)
	_, okActFile := parentAct.Items[nameAct]
	_, okActNode := parentAct.AvNodes[nameAct]
	_, okToFile  := parentTo.Items[nameTo]
	_, okToNode  := parentTo.AvNodes[nameTo]

	switch {
	case okActFile && okToFile:
		ActFile := parentAct.Items[nameAct]
		ToFile := parentTo.Items[nameTo]
		if folderOrFileExist(ActFile.Existence) && folderOrFileExist(ToFile.Existence){
			k := ToFile.Existence[len(ToFile.Existence) - 1]
			ToFile.History[[2]int{k, 0}] = append(ToFile.History[[2]int{k, 0}], id)
			ToFile.PointerOrigin = ActFile
			return
		}
		log.Fatalf("file %s or file %s does not exist in the current revision", ActFile.Name, ToFile.Name)
	case okActFile && okToNode:
		ActFile := parentAct.Items[nameAct]
		ToFolder := parentTo.AvNodes[nameTo]
		if folderOrFileExist(ActFile.Existence) && folderOrFileExist(ToFolder.Existence) {
			item := &filesystem.Item{
				Name: ActFile.Name,
				History: map[[2]int][]int{
					[2]int{id, 0}: []int{id},
				},
				PointerOrigin: ActFile,
				Existence:     []int{id},
			}
			ToFolder.Items[item.Name] = item
			return
		}
		log.Fatalf("file %s or folder %s does not exist in the current revision", ActFile.Name, ToFolder.Name)
	case okActNode && okToNode:
		ActFolder := parentAct.AvNodes[nameAct]
		ToFolder  := parentTo.AvNodes[nameTo]
		if folderOrFileExist(ActFolder.Existence) && folderOrFileExist(ToFolder.Existence) {
			ToFolder.PointerOrigin = ActFolder
			return
		}
		log.Fatalf("folder %s or folder %s does not exist in the current revision", ActFolder.Name, ToFolder.Name)
	case okActNode && okToFile:
		log.Fatalln("cannot copy folder to file")
	default:
		log.Fatalln("unforeseen behavior")
	}
}

//mvRevision - перемещает папку или файл. Делает cp и del для исходного файла или папки. Ограничения, как и в cp
func mvRevision(root *filesystem.Node, a *Action, id int) {
	cpRevision(root, a, id)
	delRevision(root, a, id)
}

//Printer удобно записывает все дерево в файл из требуемой папки
func Printer(currNode *filesystem.Node) {
	b := strings.Builder{}
	buf := bytes.Buffer{}
	fmt.Print("\nSTRUCTURE TREE\n\n")
	recPrinter(currNode, &b)
	_, errWS := buf.WriteString(b.String())
	if errWS != nil {
		log.Fatalln(errWS)
	}
	_, errB := buf.WriteTo(os.Stdin)
	if errB != nil {
		log.Fatalln(errB)
	}
}

func recPrinter(currNode *filesystem.Node, b *strings.Builder) {
	printerItem := func(file *filesystem.Item) {
		if !folderOrFileExist(file.Existence) {
			fmt.Fprint(b, "DELETE\n")
		}
		fmt.Fprintf(b, "File with name %s\n", file.Name)
		fmt.Fprintf(b, "History:\t%#v\n", file.History)
		fmt.Fprintf(b, "PointerOrigin:\t%#v\n", file.PointerOrigin)
		fmt.Fprintf(b, "Existence:\t%#v\n", file.Existence)
	}
	printerFolder := func(folder *filesystem.Node) {
		if !folderOrFileExist(folder.Existence) {
			fmt.Fprint(b, "DELETE\n")
		}
		fmt.Fprintf(b, "Folder with name %s\n", folder.Name)
		fmt.Fprintf(b, "Existence:\t%#v\n", folder.Existence)
		fmt.Fprintf(b, "Items:\t%#v\n", folder.Items)
		fmt.Fprintf(b, "PointerOrigin:\t%#v\n", folder.PointerOrigin)
		fmt.Fprintf(b, "AvNodes:\t%#v\n", folder.AvNodes)
	}
	printerFolder(currNode)
	fmt.Fprintf(b, "\nITEMS for folder with name %s\n\n", currNode.Name)
	for _, file := range currNode.Items {
		printerItem(file)
	}
	fmt.Fprintf(b, "\nAvNodes for folder with name %s\n\n", currNode.Name)
	for _, childNode := range currNode.AvNodes {
		recPrinter(childNode, b)
	}
}

func TESTPARSING() {
	root := &filesystem.Node{
		Name:          "root",
		Existence:     []int{0},
		Items:         map[string]*filesystem.Item{},
		PointerOrigin: NILNode,
		AvNodes:       map[string]*filesystem.Node{},
	}
	a := &Action{
		Operation: "add",
		FileAct:   "file1",
		Type:      "file",
		FileTo:    "",
	}
	addRevision(root, a, 1)
	Printer(root)
}
