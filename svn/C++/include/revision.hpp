#pragma once

#include <vector>
#include <string>
#include <stdio.h>
#include <node.hpp>
#include <utility>

struct Action final
{
    std::string Operation;
    std::string FileAct;
    std::string Type;
    std::string FileTo;
};


struct Revision final
{
    int                 Id;
    std::vector<Action> Actions;
};


enum class Error
{
    Succes,
    BadTypeOperation,
    NodeDontExistInCurrRevision,
    NodeDontExistInOriginNode,
    NodeDontExist,
    FileDontExistInTheCurrentRevision,
    ObjectDoesNotExistInCurrRevision,
    ObjectDoesNotExist,
    UnforeseenBehavior,
    ConnotCopyFolderToFile,
    WrongInputInRevMoreFinRev,
    WrongRequestFinRevMoreCurrRevision,
    NoValidInterval,
    WrongFolderName,
    FolderDontExistInInterval,
    FileDontExist,
    FileDontExistInInterval
};
extern std::vector<int> revisions;
extern int CURREVISION;
extern Node* NILNode;
extern Item* NILFile;
extern string File;

// for rev_parsing

void addRevision(Node*, Action&, int);
void modRevision(Node*, Action&, int);
void delRevision(Node*, Action&, int);
void cpRevision(Node*, Action&, int);
void mvRevision(Node*, Action&, int);

std::pair<Node*, std::string> getParentAndName(Node*, std::string, int);
void deleteFile(Item*, int);
void deleteFolder(Node*, int, string);
Node* findNode(Node*, string, int);

void ParsingRevisionsToTree(Node*);
void printer(Node*);


//for work_to_tree

string Path(Node*, string, string="");
void prePath(Node*, string, string);
bool NodeExistInInterval(Node*, Node*, int, int);
bool FileExistInInterval(Item*, int, int);
void FileGiveRevisions(Item*, int, int, std::vector<int>&);
void NodeGiveRevisions(Node*, Node*, int, int, std::vector<int>&);
bool FileExistKeyHistory(std::pair<int, int>, int, int);
void castRightSlice(std::vector<int>&);
void GiveRevisionsInNode(Node*, Node*, int, int, std::vector<int>&);
