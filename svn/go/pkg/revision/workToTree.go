package revision

import (
	"bytes"
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"

	"svn/pkg/filesystem"
)

var (
	revisions []int
	root      = &filesystem.Node{
		Name:          "root",
		Existence:     []int{0},
		Items:         map[string]*filesystem.Item{},
		PointerOrigin: NILNode,
		AvNodes:       map[string]*filesystem.Node{},
	}
)

//Path - приводит вывод prePath к нужному виду после его выполнения
func Path(path string, interval string) string {
	prePath(path, interval)
	revisions = castRightSlice(revisions)

	var b bytes.Buffer
	for _, v := range revisions {
		b.Write([]byte(" " + strconv.Itoa(v)))
	}
	return b.String()[1:]
}


func prePath(path string, interval string) {
	currNode := root
	var (
		inRev  int
		finRev int
	)
	if interval != "" {
		fullInterval := strings.Split(interval, "-")
		switch len(fullInterval) {
		case 2:
			i, err := strconv.Atoi(fullInterval[0])
			if err != nil {
				log.Fatalf("%s\n", err)
				return
			}
			inRev = i
			j, err := strconv.Atoi(fullInterval[1])
			if err != nil {
				log.Fatalf("%s\n", err)
				return
			}
			finRev = j
			if inRev > finRev {
				log.Fatalf("wrong input. %d > %d\n", inRev, finRev)
			}
			if finRev > CURREVISION {
				log.Fatalf("wrong request. %d > current revision %d\n", finRev, CURREVISION)
			}
		case 1:
			j, err := strconv.Atoi(fullInterval[0])
			if err != nil {
				log.Fatalf("%s\n", err)
				return
			}
			finRev = j
			inRev = 1
			if inRev > finRev {
				log.Fatalf("wrong input. %d > %d\n", inRev, finRev)
			}
			if finRev > CURREVISION {
				log.Fatalf("wrong request. %d > current revision %d\n", finRev, CURREVISION)
			}
		default:
			log.Fatalf("%s\n", "no valid interval. Example valid 53-74")
			return
		}
	} else {
		inRev = 1
		finRev = CURREVISION
	}
	path = "root/" + path
	names := strings.Split(path, "/")
	limiter := len(names) - 1
	flagForFile := false
	for i, name := range names {
		if currNode.Name != name {
			if !flagForFile {
				log.Fatalf("Node name is %s, want %s\n", currNode.Name, name)
			}
			return
		}
		if !NodeExistInInterval(currNode, inRev, finRev) {
			log.Fatalf("folder with name %s is not existence in this interval", currNode.Name)
			return
		}
		if i < limiter {
			if _, ok := currNode.AvNodes[names[i+1]]; !ok {
				if _, ok := currNode.Items[names[i+1]]; !ok {
					log.Fatalf("File with name's %s is not exist\n", names[i+1])
					return
				} else {
					if !FileExistInInterval(currNode.Items[names[i+1]], inRev, finRev) {
						log.Fatalf("file with name %s is not existence in this interval", names[i+1])
						return
					}
					flagForFile = true
					FileGiveRevisions(currNode.Items[names[i+1]], inRev, finRev, &revisions)
					return
				}
			} else {
				currNode = currNode.AvNodes[names[i+1]]
			}
		}
	}
	NodeGiveRevisions(currNode, inRev, finRev, &revisions)
	return
}

//NodeExistInInterval - функция проверки, что папка существует на заданном интервале
func NodeExistInInterval(node *filesystem.Node, inRev int, finRev int) bool {
	exist := false
	if node == root {
		exist = true
		return exist
	}
	if len(node.Existence) == 1 {
		first := node.Existence[0]
		if finRev >= first {
			exist = true
		}
		return exist
	}
	for i := 0; i < len(node.Existence); i += 2 {
		first, last := node.Existence[i], node.Existence[i+1]
		if finRev >= first && inRev <= last {
			exist = true
		}
	}
	return exist
}

//FileExistInInterval - функция проверки, что файл существует на заданном интервале
func FileExistInInterval(file *filesystem.Item, inRev int, finRev int) bool {
	exist := false
	if len(file.Existence) == 1 {
		first := file.Existence[0]
		if finRev >= first {
			exist = true
		}
		return exist
	}
	for i := 0; i < len(file.Existence); i += 2 {
		first, last := file.Existence[i], file.Existence[i+1]
		if finRev >= first && inRev <= last {
			exist = true
		}
	}
	return exist
}

//FileGiveRevisions - функция добавляет ревизии файла к слайсу ревизий. Учитывает, что файл мог быть скопирован или перемещен
func FileGiveRevisions(file *filesystem.Item, inRev int, finRev int, revisions *[]int) {
	for interval, revs := range file.History {
		if FileExistKeyHistory(interval, inRev, finRev) {
			for _, rev := range revs {
				if rev >= inRev && rev <= finRev {
					*revisions = append(*revisions, rev)
				}
			}
		}
	}
	if file.PointerOrigin != NILFile {
		FileGiveRevisions(file.PointerOrigin, inRev, file.Existence[0], revisions)
	}
}

//NodeGiveRevisions - функция добавляет ревизии всех файлов к слайсу ревизий. Учитывает, что папка могла быть скопирована или перемещена
func NodeGiveRevisions(node *filesystem.Node, inRev int, finRev int, revisions *[]int) {
	for _, file := range node.Items {
		FileGiveRevisions(file, inRev, finRev, revisions)
	}
	if node.PointerOrigin != NILNode {
		NodeGiveRevisions(node.PointerOrigin, inRev, node.Existence[0], revisions)
	}
}

//FileExistHistory проверяет, принадлежит ли заданный ключ в history нужному интервалу ревизий или нет
func FileExistKeyHistory(key [2]int, inRev int, finRev int) bool {
	if key[1] == 0 {
		if key[0] <= finRev {
			return true
		}
	}
	if finRev >= key[0] && inRev <= key[1] {
		return true
	}
	return false
}

//castRightSlice удаляет повторяющие элементы и сортирует их в обратной порядке
func castRightSlice(revisions []int) []int {
	helper := make(map[int]bool)
	for _, rev := range revisions {
		helper[rev] = true
	}
	var rawRevisions []int
	for k, _ := range helper {
		rawRevisions = append(rawRevisions, k)
	}
	sort.SliceStable(rawRevisions, func(i, j int) bool { return rawRevisions[i] > rawRevisions[j] })
	return rawRevisions
}

//TEST тестирует работу существования папок и файлов. Также тестит проверку валидности path по отдаче ревизий
func TESTREVISION() {
	CURREVISION = 10
	var (
		nodef1 *filesystem.Node
		nodef2 *filesystem.Node
	)

	f1File1 := &filesystem.Item{
		Name:          "f1File1",
		History:       map[[2]int][]int{[2]int{4, 10}: []int{4, 5, 6, 10}},
		Existence:     []int{4, 10},
		PointerOrigin: NILFile,
	}

	f1f3File1 := &filesystem.Item{
		Name:          "f1f3File1",
		History:       map[[2]int][]int{[2]int{9, 0}: []int{9, 10}},
		Existence:     []int{9},
		PointerOrigin: f1File1,
	}

	f1f3File2 := &filesystem.Item{
		Name:          "f1f3File2",
		History:       map[[2]int][]int{[2]int{9, 0}: []int{9, 10}},
		Existence:     []int{9},
		PointerOrigin: NILFile,
	}

	nodef1f3 := &filesystem.Node{
		Name:          "f3",
		Existence:     []int{9},
		Items:         map[string]*filesystem.Item{"file1": f1f3File1, "file2": f1f3File2},
		PointerOrigin: nodef2,
		AvNodes:       map[string]*filesystem.Node{},
	}

	f1File2 := &filesystem.Item{
		Name:          "f1File2",
		History:       map[[2]int][]int{[2]int{4, 0}: []int{4, 5, 6}},
		Existence:     []int{4},
		PointerOrigin: NILFile,
	}
	nodef1 = &filesystem.Node{
		Name:          "f1",
		Existence:     []int{4},
		Items:         map[string]*filesystem.Item{"file1": f1File1, "file2": f1File2},
		PointerOrigin: NILNode,
		AvNodes:       map[string]*filesystem.Node{"f3": nodef1f3},
	}

	f2File3 := &filesystem.Item{
		Name:          "f2File3",
		History:       map[[2]int][]int{[2]int{6, 9}: []int{6, 8, 9}},
		Existence:     []int{6, 9},
		PointerOrigin: NILFile,
	}

	f2f1File1 := &filesystem.Item{
		Name:          "f2f1File1",
		History:       map[[2]int][]int{[2]int{7, 9}: []int{7, 9}},
		Existence:     []int{7, 9},
		PointerOrigin: NILFile,
	}
	f2f1File2 := &filesystem.Item{
		Name:          "f2f1File2",
		History:       map[[2]int][]int{[2]int{7, 9}: []int{7, 9}},
		Existence:     []int{7, 9},
		PointerOrigin: NILFile,
	}

	nodef2f1 := &filesystem.Node{
		Name:          "f1",
		Existence:     []int{7, 9},
		Items:         map[string]*filesystem.Item{"file1": f2f1File1, "file2": f2f1File2},
		PointerOrigin: nodef1,
		AvNodes:       map[string]*filesystem.Node{},
	}

	nodef2 = &filesystem.Node{
		Name:          "f2",
		Existence:     []int{6, 9},
		Items:         map[string]*filesystem.Item{"file3": f2File3},
		PointerOrigin: NILNode,
		AvNodes:       map[string]*filesystem.Node{"f1": nodef2f1},
	}
	root.AvNodes = map[string]*filesystem.Node{"f1": nodef1, "f2": nodef2}

	result := Path("f2/f1", "")
	fmt.Println(result)
}
